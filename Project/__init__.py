from distutils.debug import DEBUG
from flask import Flask, request, abort
import requests
import json
import random
import tflearn
import numpy
import nltk
import pickle
from tensorflow.python.framework import ops
from nltk.stem.lancaster import LancasterStemmer
from linebot import LineBotApi
import PIL.Image as Image
import io
import os
from linebot.models import (
    LocationSendMessage, TextSendMessage, QuickReply, QuickReplyButton, LocationAction, CameraAction, FlexSendMessage
)
from pythainlp.tokenize import word_tokenize
from pythainlp.corpus import thai_stopwords
import datetime


# ถ้ามีเวลาเหลือ ทำที่แอดคำใหม่เข้าไปด้วย จากฝั่ง admins
# แล้วถ้าเจอคำใหม่ๆที่ไม่อยู่ใน dataset เลย แต่ความหมายมันลื่อในปัญหานั้น จะทำยังไงให้มันรู้


# ที่อยู่รูปที่บันทึก
dir_path = 'C:/Users/thanasaksomroob/Desktop/testssss/testbot/pic'

# ข้อมูล Line developers chatbot
Channel_access_token = 'kDT/5enzQDKjpq4uoMqlR9gMXundmCi1AAalqGVVkiV7oZOwQ0cPWOwT7f2Xoy+td4QJec6AYPiLyMH3HYs0RXYcC82N0qSsLn/SHFoWp1NOyIGNcZ8wAW51d7BZhGLcZ/HBflHCehNf4dJlirS0MQdB04t89/1O/w1cDnyilFU='
Channel_secret = '82e05412afa9c30fe70f7cc2e4637ebc'
basic_id = '@857ebjud'
LINE_API = 'https://api.line.me/v2/bot/message/reply'
line_bot_api = LineBotApi(Channel_access_token)

# run Flask
app = Flask(__name__)
print("name",   app)


# เก็บ state เพื่อให้บอทรู้ว่า เป็น location ของ case ไหน 
state_tag = ""

# webhook ส่งข้อมูลกับ Line
@app.route('/webhook', methods=['POST', 'GET'])
def webhook():
    
    if request.method == 'POST':
        # print(request.json)
        dataload = request.json
        print(dataload)
        # ทางไลน์ส่งมา
        Reply_token = dataload['events'][0]['replyToken']
        message_type = dataload['events'][0]['message']['type']
        if message_type == 'text':
            message = dataload['events'][0]['message']['text']
            # print(message)
            inp = message
            """ if inp.lower() == "quit":
                return  """
            results = model.predict([bag_of_word(inp, words)])
            results_index = numpy.argmax(results)
            print("re", results)
            print("max", results_index)
            print(labels)
            tag = labels[results_index]
            for tg in data["intents"]:
                if tg['tag'] == tag:
                    # บันทึก patterns ใหม่ที่ได้จาก line เอาไว้ใช้ครั้งต่อไป
                    if tag == "greeting":
                        with open("intents.json", "r+", encoding="utf-8") as file:
                            new_patterns = json.load(file)
                            file_data = inp
                            # เดี๋ยวมาแก้ตรงนี้ต่อ
                            """ if file_data not in data["intents"][0]["patterns"]:
                                new_patterns["intents"][0]["patterns"].append(file_data)
                                file.seek(0)
                                json.dump(new_patterns, file, ensure_ascii=False, indent=3) """
                    elif tag == "flood problem":
                        with open("intents.json", "r+", encoding="utf-8") as file:
                            new_patterns = json.load(file)
                            file_data = inp
                            """ if file_data not in data["intents"][1]["patterns"]:
                                new_patterns["intents"][1]["patterns"].append(file_data)
                                file.seek(0)
                                json.dump(new_patterns, file, ensure_ascii=False, indent=3) """
                    elif tag == "road problem":
                        with open("intents.json", "r+", encoding="utf-8") as file:
                            new_patterns = json.load(file)
                            file_data = inp
                            """ if file_data not in data["intents"][2]["patterns"]:
                                new_patterns["intents"][2]["patterns"].append(file_data)
                                file.seek(0)
                                json.dump(new_patterns, file, ensure_ascii=False, indent=3) """
                    elif tag == "electrical problem":
                        with open("intents.json", "r+", encoding="utf-8") as file:
                            new_patterns = json.load(file)
                            file_data = inp
                            """ if file_data not in data["intents"][3]["patterns"]:
                                new_patterns["intents"][3]["patterns"].append(file_data)
                                file.seek(0)
                                json.dump(new_patterns, file, ensure_ascii=False, indent=3) """
                    elif tag == "garbage problem":
                        with open("intents.json", "r+", encoding="utf-8") as file:
                            new_patterns = json.load(file)
                            file_data = inp
                            """ if file_data not in data["intents"][4]["patterns"]:
                                new_patterns["intents"][4]["patterns"].append(file_data)
                                file.seek(0)
                                json.dump(new_patterns, file, ensure_ascii=False, indent=3) """
                    responses = tg['responses']
                    re_m = random.choices(responses)
                    Reply_message = ' '.join([str(elem) for elem in re_m])
                    print("RE", Reply_message)
                    if tg['tag'] == "greeting":
                        greeting(Reply_token, Reply_message)
                        # ReplyMessage(Reply_token,Reply_message,Channel_access_token,message_type)
                    elif tg['tag'] == "flood problem" or tg['tag'] == "road problem" or tg['tag'] == "electrical problem" or tg['tag'] == "garbage problem":
                        webhook.state_tag = tg['tag']
                        Camera_Action(Reply_token, Reply_message)
                    elif tg['tag'] == "no_picture":
                        webhook.state_tag = tg['tag']
                        Quick_Reply(Reply_token)
                    elif tg['tag'] == "track_problems":
                        # ยังติดปัญหาว่าจะเอาหมายเลขที่ส่งมาออกมาใช้ยังไง
                        sendtextunderstand(Reply_token, Reply_message)
                    elif tg['tag'] == "PM2.5":
                        webhook.state_tag = tg['tag']
                        responses = tg['responses']
                        re_m = random.choices(responses)
                        Reply_message = ' '.join([str(elem) for elem in re_m])
                        Quick_Reply_PM(Reply_token, Reply_message)
                        
            print(state_tag)
            print(tag)
            print(results_index)
            print(Reply_message)
            ReplyMessage(Reply_token, Reply_message,
                         Channel_access_token, message_type)
        elif message_type == 'location':
                if (webhook.state_tag == "PM2.5"):
                    # api air4thai
                    response_API = requests.get('http://air4thai.pcd.go.th/services/getNewAQI_JSON.php?')
                    data_api = response_API.text 
                    #d_api = json.loads(data_api)
                    data_location = dataload['events'][0]['message']['address']
                    lat = '{}'.format(dataload['events'][0]['message']['latitude'])
                    long = '{}'.format(dataload['events'][0]['message']['longitude'])
                    #std = '{}'.format(dataload['events'][0]['message']['stationID'])
                    f = open('dataapi.json')
                    dataapi = json.load(f)
                    #ทำเพื่อค่า stationID     stationID ก็คือเครื่องวัด PM2.5 ใน air4thai
                    def in_dictlist(lat, value_lat,long,value_long ,my_dictlist):
                        for entry in my_dictlist:
                            # กรณีที่ lat long ตรงกับตำแหน่ง stationID
                            if (entry[lat] == value_lat) and (entry[long] == value_long):
                                return entry['stationID']
                            # กรณีไม่ตรง ต้องค่าจากอันใกล้สุด
                            else:
                                lat_near_new = 0
                                long_near_new = 0
                                stations_ID = 0
                                for s in my_dictlist:
                                    if (lat_near_new == 0) and (long_near_new == 0) and (stations_ID == 0):
                                        lat_near_new = s['lat']
                                        long_near_new = s['long']
                                        station_ID = 0
                                    elif (lat_near_new != 0) and (long_near_new != 0):
                                        v_lat = abs(float(s['lat']) - float(value_lat))
                                        v_long = abs(float(s['long']) - float(value_long))  
                                        v_lat_new = abs(float(lat_near_new) - float(value_lat))  
                                        v_long_new = abs(float(long_near_new) - float(value_long))
                                        if (v_lat < v_lat_new) and (v_long < v_long_new):
                                            lat_near_new = s['lat']
                                            long_near_new = s['long']
                                            station_ID = s['stationID']
                        return station_ID        
                    s_id = (in_dictlist('lat',lat,'long',long, dataapi))
                    response_API = requests.get('http://air4thai.pcd.go.th/services/getNewAQI_JSON.php?stationID={}'.format(s_id))
                    data_res = response_API.text 
                    data_api = json.loads(data_res)
                    for d_api in data_api:
                        Pm25 = data_api['LastUpdate']['PM25']['value'] + data_api['LastUpdate']['PM25']['unit']
                        Date = data_api['LastUpdate']['date'] 
                        Time = data_api['LastUpdate']['time']
                        Aqi =  data_api['LastUpdate']['AQI']['aqi']
                        original_date = datetime.datetime.strptime("{}".format(Date), '%Y-%m-%d')
                        formatted_date = original_date.strftime("%d:%m:%Y")
                    Reply_message =  Pm25 + formatted_date + Time + Aqi + data_location + s_id
                    ReplyMessage(Reply_token, Reply_message,Channel_access_token, message_type)
                    webhook.state_tag == ""
                elif (webhook.state_tag == "no_picture"):
                    message_type = dataload['events'][0]['message']['type']
                    Reply_message = 'ขอบคุณสำหรับปัญหาที่แจ้งมาทางเราจะดำเนินการอย่างเร่งด่วน'
                    ReplyMessage(Reply_token, Reply_message,
                                Channel_access_token, message_type)
                    webhook.state_tag == ""   
                else:
                    message_type = dataload['events'][0]['message']['type']
                    Reply_message = 'ขอบคุณสำหรับปัญหาที่แจ้งมาทางเราจะดำเนินการอย่างเร่งด่วน'
                    ReplyMessage(Reply_token, Reply_message,
                                Channel_access_token, message_type)          
                       

        elif message_type == 'image':
            #message_id = dataload['events'][0]['message']['id']
            #b = []
            #arr_mid_m = []
            for x in dataload['events']:
                arr_mid_m = []
                arr_mid_m.append(x['message']['id'])
                message_id = x['message']['id']
                message_contents = line_bot_api.get_message_content(
                    x['message']['id'])
                with open('file_path', 'wb') as fd:
                    for chunk in message_contents.iter_content(chunk_size=720*720):
                        # ขนาดอันรูปอันก่อน 1024*1024
                        fd.write(chunk)
                with open('file_path', mode='rb') as file:
                    filepath = file.read()
                    img = Image.open(io.BytesIO(filepath))
                    # img.show()

                print(arr_mid_m)
                filename = 'pic{}.jpg'.format(message_id)
                file_path_save = os.path.join(dir_path, filename)
                img.save(file_path_save)

            # มีไวเพื่อล้างข้อมูลในไฟล์ file_path
            with open('file_path', 'wb') as fd:
                fd.close()
            # location action
            Quick_Reply(Reply_token)
        return request.json, 200
    elif request.method == 'GET':

        return 'this is method GET!! เอาไว้ส่งข้อมูลไปมากับไลน์', 200
    else:
        abort(400)


@app.route('/')
def Hello():
    return 'Hello', 200


def sendtextunderstand(Reply_token, TextMessage):
    line_bot_api.reply_message(
        Reply_token,
        TextSendMessage(text=TextMessage))


def Quick_Reply(Reply_token):
    line_bot_api.reply_message(
        Reply_token,
        TextSendMessage(
            text='แชร์ที่อยู่บริเวณนั้นมาหน่อยครับ',
            quick_reply=QuickReply(
                items=[
                    QuickReplyButton(
                        action=LocationAction(label="Send location")
                    ),
                ])))


def Quick_Reply_PM(Reply_token, Reply_message):
    line_bot_api.reply_message(
        Reply_token,
        TextSendMessage(
            text=Reply_message,
            quick_reply=QuickReply(
                items=[
                    QuickReplyButton(
                        action=LocationAction(label="Send location")
                    ),
                ])))


def Camera_Action(Reply_token, TextMessage):
    line_bot_api.reply_message(
        Reply_token,
        TextSendMessage(
            text=TextMessage,
            quick_reply=QuickReply(
                items=[
                    QuickReplyButton(
                        action=CameraAction(label="กล้องถ่ายรูป")
                    )
                ])))


def greeting(Reply_token, TextMessage):
    with open("bubble_greeting.json", encoding="utf-8") as file:
        data = json.load(file)
        bubble_string =  '{}'.format(data)  
        bbs = bubble_string.replace("\'", "\"").replace("True", "true")
    message = FlexSendMessage(
        alt_text="problem", contents=json.loads(bbs))
    messages = [
        message,
        TextSendMessage(text=TextMessage)
    ]
    line_bot_api.reply_message(Reply_token, messages)


def ReplyMessage(Reply_token, TextMessage, Line_Acees_Token, message_type):
    #LINE_API = 'https://api.line.me/v2/bot/message/reply'

    Authorization = 'Bearer {}'.format(Line_Acees_Token)  # ที่ยาวๆ
    # print(Authorization)

    if (message_type == 'text'):
        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': Authorization
        }
        data = {
            "replyToken": Reply_token,
            "messages": [
                {
                    "type": "text",
                    "text": TextMessage
                }]
        }
        data = json.dumps(data, ensure_ascii=False)
        requests.post(LINE_API, headers=headers, data=data.encode("utf-8"))

    elif (message_type == 'location'):
        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': Authorization
        }
        data = {
            "replyToken": Reply_token,
            "messages": [
                {
                    "type": "text",
                    "text": TextMessage
                }]
        }
        data = json.dumps(data, ensure_ascii=False)
        r = requests.post(LINE_API, headers=headers, data=data.encode("utf-8"))
        print(data)
        print(r)

    return 200


stemmer = LancasterStemmer()
with open("intents.json", encoding="utf-8") as file:
    data = json.load(file)

with open("data.pickle", "rb") as f:
    words, labels, training, output = pickle.load(f)
words = []
labels = []
docs_x = []
docs_y = []
for intent in data["intents"]:
    for pattern in intent["patterns"]:
        #wrds = nltk.word_tokenize(pattern)
        wrds = word_tokenize(pattern, engine='newmm')
        words.extend(wrds)
        # คำ
        docs_x.append(wrds)
        # tag
        docs_y.append(intent["tag"])

    if intent["tag"] not in labels:
        labels.append(intent["tag"])
print(docs_x)  # คำ
print(docs_y)  # หมวดหมู่
#print ("%s sentences of training data" % len(training_data))
# print(training_data)
print(labels)  # มันคือ tag
words = [stemmer.stem(w.lower()) for w in words if w != "?"]
words = sorted(list(set(words)))  # คำต่างๆ
labels = sorted(labels)
training = []
output = []
out_empty = [0 for _ in range(len(labels))]
for x, doc in enumerate(docs_x):
    bag = []
    wrds = [stemmer.stem(w.lower()) for w in doc]
    for w in words:
        if w in wrds:
            bag.append(1)
        else:
            bag.append(0)
    output_row = out_empty[:]
    output_row[labels.index(docs_y[x])] = 1
    training.append(bag)
    output.append(output_row)
    # print(training)

    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, training, output), f)
training = numpy.array(training)
output = numpy.array(output)
print(len(training[0]))
print(training)
print(output)
ops.reset_default_graph()

# Build neural network
net = tflearn.input_data(shape=[None, len(training[0])])
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, 8)
net = tflearn.fully_connected(net, len(output[0]), activation="softmax")
# ใช้ regression
net = tflearn.regression(net, optimizer='RMSprop')
model = tflearn.DNN(net)
model.fit(training, output, n_epoch=5, batch_size=8, show_metric=True)
model.save("model.tflearn")
model.load("model.tflearn")


# Training Step มาจาก จำนวนข้อมูลทั้งหมด/batch_size
# n_epoch จำนวนรอบในการ train


# s คือ inp , words คือ คำของ model
def bag_of_word(s, words):
    bag = [0 for _ in range(len(words))]
    # print(bag)
    #s_words = word_tokenize(s)
    s_words = word_tokenize(s)
    print("s_words")
    print(s_words)
    s_words = [stemmer.stem(word.lower()) for word in s_words]
    for se in s_words:
        # enumerate เป็นคำสั่งสำหรับแจกแจงค่า index
        #จะได้ (Index,Value)
        for i, w in enumerate(words):
            if w == se:
                bag[i] = (1)
                print(se)
    # ตรงนี้เทียบเอาถ้าต้องกับ bag แล้วใส่ 1
    print("bag")
    print(len(numpy.array(bag)))
    print("sdsds", numpy.array(bag))
    print(numpy.argmax(bag))
    return numpy.array(bag)
